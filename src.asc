= src(1) =
:doctype: manpage

== NAME ==
src - simple revision control

== SYNOPSIS ==
*src* [command] [revision-spec] ['file'...]

== DESCRIPTION ==

SRC (or src) is simple revision control, a version-control system for
single-file projects by solo developers and authors.  It modernizes the
venerable RCS, hence the anagrammatic acronym.  The design is tuned for
use cases like all those little scripts in your "~/bin" directory, or a
directory full of single-file HOWTOs.

SRC revision histories are single, human-readable files beneath a hidden
".src" subdirectory in the directory where they live.  There may be
multiple histories under one directory; SRC treats them as separate
projects, and history files can be moved elsewhere at any time.

SRC gives you simple, consecutive integer revision numbers.  It supports
tags and branching.  It does not show committer information, because the
committer is always you.  The command set is intended to look familiar
if you have ever used Subversion, Mercurial, or Git.

SRC interprets the EDITOR variable in the usual way, using it to spawn
an editor instance when you perform a commit or amend.

SRC is fully supported in Emacs VC mode.

== COMMAND SUMMARY ==

A "revision" is a 1-origin integer, or a tag name designating an integer
revision, or a branch name designating the tip revision of its branch.
Revision numbers always increase in commit-date order.

A revision range is a single revision, or a pair of revisions "M-N" (all
revisions numerically from M to N) or "M..N" (all revisions that are
branch ancestors of N and branch successors of M).

If SRC complains that your revision spec looks like a nonexistent
filename, you can prefix it with "@" (this is always allowed). "@" by
itself means the current (checked-out) revision.

Unless otherwise noted under individual commands, the default revision
is the tip revision on the current branch and the default range is all
revisions on the current branch.

The token "--" tells the command-line interpreter that subcommands,
switches, and revision-specs are done - everything after it is a
filename, even if it looks like a subcommand or revision number.

src help ['command']::
    Displays help for commands.

src commit [-|-m 'string'|-f 'file'|-e] ['file'...]::
    Enters a commit for specified files.  Separately to each one.  A
    history is created for the file if it does not already exist.  With
    '-', take comment text from stdin; with '-m' use the following
    string as the comment; with '-f' take from a file.  With '-e', edit
    even after '-', '-f' or '-m'. "ci" is a synonym for "commit".

src amend [-|-m 'string'|-f 'file'|-e] ['revision'] ['file'...]::
    Amends the stored comment for a specified revision, defaulting to
    the latest revision on the current branch.  Flags are as for commit.
    "am" is a synonym for "amend".

src checkout ['revision'] ['file'...]::
    Refresh the working copy of the file(s) from their history files.
    "co" is a synonym for "checkout".

src cat ['revision'] ['file'...]::
    Send the specified revision of the files to standard output.

src status [-a] ['file'...]::
    "A" = added, "=" = unmodified, "M" = modified, "!" = missing, "?"
    = not tracked, "I" = ignored, "L" = locked (recover with "src
    checkout").  The "A" and "L" statuses should only occur if you have
    used RCS directly on a file.  Normally "?" and "I" files are not
    listed; this changes if you either give the '-a' switch or specify
    which files to look at. "st" is a synonym for "status".

src tag [list|-l|create|-c|delete|del|-d] ['name'] ['revision'] ['file'...]::
    List tags, create tags, or delete tags.  Create/delete takes a
    revision, defaulting to the current branch tip.  List to all
    revisions.

src branch [list|-l|create|-c|delete|del|-d] ['name'] ['file'...]::
    List, create, or delete branches.  When listing, the active branch
    is first in the list.  The default branch is "trunk".  Create/delete
    takes a singleton revision, defaulting to the current branch tip.
    List defaults to all revisions, including 0 (the trunk root phantom
    revision).

src rename ['tag'|'branch'] ['oldname'] ['newname']  ['file'...]::
    Rename a tag or branch.  Refuses to step on an existing symbol or
    rename a nonexistent one. "rn" is a synonym for "rename".

src list [(-<n>|-l <n>)] [-f 'fmt'] ['revision-range'] ['file'...]::
    Sends summary information about the specified commits to standard
    output.  The summary line tagged with "*" is the state that the
    file would return to on checkout without a revision-spec.  See 'help
    list' for information about custom formats.  Use '-<n>' or '-l <n>',
    where <n> is a number, to limit the listing length.  Default range
    is the current branch, reversed.  "li" is a synonym for "list".

src log [-v] [(-<n>|-l <n>)] [(-p|-u|-c) [-b|-w]] ['revision-range'] ['file'...]::
    Sends log information about the specified commits to standard
    output.  Use '-<n>' or '-l <n>', where <n> is a number, to limit the
    listing length.  Default range is the current branch, reversed.
    The '--patch', '-p' or '-u' option additionally sends a
    unified format diff listing to standard output for each revision
    against its immediate ancestor revision; '-c' emits a context diff
    instead. When generating a diff, '-b' ignores changes in whitespace,
    and '-w' ignores all whitespace.  Histories imported via
    'fast-import' (when not using its '-p' option) have RFC-822-style
    headers inserted into the log comment to preserve metadata not
    otherwise representable in SRC, such as distinct author and
    committer identifications and dates.  These headers are normally
    suppressed by 'log', however, '-v' shows a summarized view of
    important headers; '-v -v' shows all headers as-is.

src diff [(-u|-c) [-b|-w]] ['revision-range'] ['file'...]::
    Sends a diff listing to standard output.  With no revision spec,
    diffs the working copy against the last version checked in.  With
    one revno, diffs the working copy against that stored revision; with
    a range, diff between the beginning and end of the range.
    The actual difference generation is done with diff(1).  The default
    diff format is '-u' (unified), but if you specify a '-c' option
    after the verb a context diff will be emitted. '-b' ignores changes
    in the amount of whitespace, and '-w' ignores all whitespace.
    "di" is a synonym for "diff".

src ls::
    List all registered files.

src visualize::
    Emit a DOT visualization of repository structures.  To use this,
    install the graphviz package and pipe the output to something like
    "dot -Tpng | display -". "vis" is a synonym for "visualize".

src move 'old' 'new'::
    Rename a workfile and its history.  Refuses to step on existing
    workfiles or histories. "mv" is a synonym for "move".

src copy 'old' 'new'::
    Copy a file and its master.  Refuses to step on existing files or
    masters. "cp" is a synonym for "copy".

src fast-export ['revision-range'] ['file'...]::
    Export one or more projects as a Git fast-import stream.  For a
    history originally imported from elsewhere, author and committer
    identification is gleaned from the RFC-822-style headers inserted
    into the commit comment by 'fast-import' (if its '-p' option was not
    used).  Otherwise, this information is copied from your Git
    configuration.

src fast-import [-p] ['files'...]::
    Parse a git-fast-import stream from standard input.  The
    modifications for each individual file become separate
    SRC histories.  Mark, committer and author data, and mark
    cross-references to parent commits, are preserved in RFC-822-style
    headers on log comments unless the '-p' (plain) option is given, in
    which case this metadata is discarded.  Give arguments to restrict
    the files imported.

src release ['file'...]::
    Release locks on files.  This is never necessary in a normal
    workflow, which will be repeated edit-commit cycles, but it may be
    handy if you have to interoperate with other tools that expect RCS
    masters to be in their normal (unlocked) state.

src version::
    Report the versions of SRC, the underlying Python, and the back end.

The omission of "src remove" is a deliberate speed bump.

If no files are specified, all eligible files are operated on in
sequence.

== NOISE CONTROL ==

Silence is golden.  When you have selected only one file to operate
on, and the command is not a report generator ('status', 'cat', 'log',
'list', 'fast-export', the listing modes of 'tag' and 'branch', 'ls')
you will see a reply only if the operation failed.

Other commands ('commit', 'checkout', tag creation and deletion) give
you a success message per file when operating on multiple files, so you
will not be in doubt about which operation succeeded.  This behavior can
be suppressed with the '-q' option, placed 'before' the subcommand word.

If your directory contains a file named ".srcignore", each line that is
neither blank nor begins with a "#" is interpreted as an ignore pattern.
It is expanded with glob(3), and files in the expansion are omitted
from 'src status' - unless the file is named as an argument, of the
status command, in which case its status is "I".  Thus, for example, a
line reading "*.html" will cause all files with an HTML extension to be
omitted from the output of "src status", but the output of 'src status
*' will list them with status "I".

== BACKWARD COMPATIBILITY ==

SRC history files are (normally) RCS master files.  SRC maintains no
permanent state other than these files.

SRC takes over the little-used "description" field of RCS (and SCCS)
master files to store some state that makes status checks faster.  If
you try to use SRC to edit a pre-existing RCS- or SCCS-registered file
with a non-empty description field, SRC will refuse to step on the old
description; you must clear it manually.

In order to maintain complete backwards compatibility, one other
compromise was made: any commit comment containing a string exactly
matching an RCS log delimiter (a long string of "-" characters) will be
rejected as malformed.

The RCS back end will be automatically selected when there is an ".src"
or "RCS" subdirectory.

You can explicitly select the RCS back end by making the first command
keyword on the src command line be 'rcs'.  This should only be necessary
when your working directory contains two or more of the subdirectories
".src", "RCS", and "SCCS".

By default, history files are kept in a hidden subdirectory named
".src".  But if you have an RCS subdirectory and no ".src", SRC will
quietly operate on the files in the RCS directory in a completely
backward-compatible way.

== WORKING WITH SCCS ==

Using SCCS as a back end is also supported, with some limits due to
missing features in SCCS implementations:

* All commands relating to tags and branches throw an error.

* In CSSC SCCS, src cat does not necessarily pipe out binary data
  correctly. This has been fixed in the SunOS and Schilling versions.

* The exec bit is not propagated between master and workfile. This has
  been fixed in the SunOS and Schilling versions.

* The CSSC and SunOS SCCS backends have a Y2.1K problem that SRC
  cannot effectively work around and will probably not be fixed.  The
  Schilling version has repaired this.

The SCCS back end will be automatically selected when there is an "SCCS"
subdirectory and no ".src" or "RCS" subdirectory.

You can explicitly select the SCCS back end by making the first command
keyword on the src command line be 'sccs'.  This should only be
necessary when your working directory contains none or two or more of
the subdirectories ".src", "RCS", and "SCCS".

Working with SCCS requires an "SCCS" subdirectory; SRC will quietly
create one, if required, then operate on the files in the "SCCS"
directory in a completely backward-compatible way.

Fast-import to SCCS is not supported.

The SCCS mode is not recommended unless you have a specific need to
work with legacy SCCS repositories. Up-converting them to something
less Paleolithic would be a better idea; in truth, the SCCS support
exists mainly because it's hilarious (for obscure hacker values of
'hilarious').

== DEBUGGING OPTIONS ==

These will be of interest mainly to developers.

A '-d' (debug) option before the main command word turns on debugging
messages. Just one "-d" gives you complete visibility about what
commands the back end is running. It can be repeated for higher
debugging levels that expose more of src's internal computation.

A '-S' (sandbox) option can be used to set the repository directory to
something other than its default of ".src".

A '-T' option sets up an artificial clock that ticks once on each
revision and fixes the user information to be used in 'fast-export'; It
also attributes all commits to "J. Random Hacker".  It is for regression
testing.

== REQUIREMENTS ==

SRC is written in Python and requires 2.7 or later; it will run under
Python 3.x.

If you wish to use the RCS support, the RCS tools at version 5.7 or
later must be installed and accessible in your path.

If you wish to use the SCCS support, some implementation of SCCS must
be accessible. GNU's CSSC (Compatibly Stupid Source Control) will
work; so will the SunOS and Schilling forks of AT&T SCCS.

The rcs-fast-import(1) tool at version 1.0 or later is required to
support the 'src fast-import' command.

src will die gracefully with a useful error message when it fails
due to a missing back end.

== LIMITATIONS ==

Branch deletions change the revision numbers of revisions downstream of
the branch join.  This behavior may change in a future release.

== REPORTING BUGS ==

Report bugs to Eric S. Raymond <esr@thyrsus.com>.  The project page is
at http://catb.org/~esr/src

== SEE ALSO ==

rcs(1), rcs-fast-import(1), sccs(1), svn(1), git(1), dot(1).
